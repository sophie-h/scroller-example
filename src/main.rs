use adw::prelude::*;
use adw::Application;
use gtk::{gio, glib};

mod scroller;
mod window;

static GRESOURCE_BYTES: &[u8] =
    gvdb_macros::include_gresource_from_dir!("/com/example/ExampleApp", "data/resources");

fn main() {
    gio::resources_register(
        &gio::Resource::from_data(&glib::Bytes::from_static(GRESOURCE_BYTES)).unwrap(),
    );

    let application = Application::builder()
        .application_id("com.example.ExampleApp")
        .resource_base_path("/com/example/ExampleApp")
        .build();

    application.connect_activate(|app| {
        let window = window::ExlWindow::new(app);
        window.present();
    });

    application.run();
}
