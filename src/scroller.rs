use std::cell::{Cell, OnceCell, RefCell};

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{glib, graphene, gsk};

mod imp {
    use super::*;

    #[derive(Default, Debug, glib::Properties)]
    #[properties(wrapper_type = super::ExlScroller)]
    pub struct ExlScroller {
        #[property(get, set=Self::set_child)]
        child: RefCell<Option<gtk::Widget>>,
        #[property(get, set=Self::set_text_position)]
        text_position: Cell<f64>,

        animation: OnceCell<adw::TimedAnimation>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ExlScroller {
        const NAME: &'static str = "ExlScroller";
        type Type = super::ExlScroller;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            klass.set_css_name("exlscroller")
        }
    }

    impl ObjectImpl for ExlScroller {
        fn dispose(&self) {
            self.child.borrow().as_ref().inspect(|x| x.unparent());
        }

        fn properties() -> &'static [glib::ParamSpec] {
            Self::derived_properties()
        }

        fn set_property(&self, id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
            self.derived_set_property(id, value, pspec)
        }

        fn property(&self, id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            self.derived_property(id, pspec)
        }
    }

    impl WidgetImpl for ExlScroller {
        fn measure(&self, orientation: gtk::Orientation, for_size: i32) -> (i32, i32, i32, i32) {
            if let Some(child) = self.child.borrow().as_ref() {
                let mut measure = child.measure(orientation, for_size);

                if orientation == gtk::Orientation::Vertical {
                    let height = self.obj().height_request();
                    measure.0 = height;
                    measure.1 = height;
                }

                measure
            } else {
                (0, 0, -1, -1)
            }
        }

        fn size_allocate(&self, width: i32, height: i32, baseline: i32) {
            let obj = self.obj();
            if let Some(child) = self.child.borrow().as_ref() {
                let transform = gsk::Transform::new()
                    .translate(&graphene::Point::new(0., obj.text_position() as f32));
                child.allocate(width, height, baseline, Some(transform));
            }
        }
    }

    impl ExlScroller {
        fn set_child(&self, child: gtk::Widget) {
            self.child.borrow().as_ref().inspect(|x| x.unparent());
            child.set_parent(&*self.obj());
            self.child.replace(Some(child));
        }

        fn set_text_position(&self, position: f64) {
            self.text_position.set(position);
            self.obj().queue_allocate();
        }

        pub(super) fn animation(&self) -> adw::TimedAnimation {
            self.animation
                .get_or_init(|| {
                    let obj = self.obj().to_owned();

                    adw::TimedAnimation::builder()
                        .duration(1000)
                        .widget(&obj)
                        .target(&adw::PropertyAnimationTarget::new(&obj, "text-position"))
                        .build()
                })
                .clone()
        }
    }
}

glib::wrapper! {
    pub struct ExlScroller(ObjectSubclass<imp::ExlScroller>)
        @extends gtk::Widget;
}

impl ExlScroller {
    pub fn animate(&self, by: f64) {
        let animation = self.imp().animation();

        let current_position = self.text_position();
        animation.set_value_from(current_position);
        animation.set_value_to(current_position - by);
        animation.play();
    }
}
