use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::glib;

mod imp {
    use super::*;

    #[derive(Default, Debug, gtk::CompositeTemplate)]
    #[template(file = "window.ui")]
    pub struct ExlWindow {
        #[template_child]
        pub(super) start_animation: TemplateChild<gtk::Button>,
        #[template_child]
        pub(super) scroller: TemplateChild<crate::scroller::ExlScroller>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ExlWindow {
        const NAME: &'static str = "ExlWindow";
        type Type = super::ExlWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ExlWindow {
        fn constructed(&self) {
            self.parent_constructed();

            let scroller = self.scroller.to_owned();
            self.start_animation.connect_clicked(move |_| {
                // This has to be the line-height
                scroller.animate(35.);
            });
        }
    }
    impl WidgetImpl for ExlWindow {}
    impl WindowImpl for ExlWindow {}
    impl ApplicationWindowImpl for ExlWindow {}
    impl AdwApplicationWindowImpl for ExlWindow {}
}

glib::wrapper! {
    pub struct ExlWindow(ObjectSubclass<imp::ExlWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gtk::Native;
}

impl ExlWindow {
    pub fn new(app: &impl IsA<adw::Application>) -> Self {
        glib::Object::builder().property("application", app).build()
    }
}
